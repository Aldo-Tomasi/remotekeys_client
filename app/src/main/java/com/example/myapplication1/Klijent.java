package com.example.myapplication1;

import android.text.TextUtils;
import android.util.Patterns;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Klijent {

    private static Socket sock;
    private static ObjectOutputStream objOutput;
    private static ObjectInputStream objInput;



    public static void connectToServer(String IP_adr,int port) {

        try {
            sock = new Socket(IP_adr, port);
            streamSetup();
            System.out.println("Socket set up and streams working\n");

        } catch (IOException e) {
            System.out.println("Kriva adresa ili nedozvoljeni port!");
            System.out.println(e.getMessage());
        }

    }

    //set up streams
    public static void streamSetup(){
        try {
            objOutput = new ObjectOutputStream(sock.getOutputStream());
        } catch (IOException e) {
            System.out.println("ObjOutputStream error!");
        }
        try {
            objOutput.flush();
        } catch (IOException e) {
            System.out.println("Error while flushing!");
        }
        /*try {
            objInput = new ObjectInputStream(sock.getInputStream());
        } catch (IOException e) {
        	System.out.println("ObjInputStream error!");
        }*/

    }

    public static void sendObject (String methodCode) {
        try {


            objOutput.writeObject(methodCode);
            objOutput.flush();
            System.out.println("\nObject successfully sent");

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("\nError sending message");
        }

    }

    public static boolean validateIP(String ip) {   //check if ip address is valid
        Pattern patternEmail = Patterns.IP_ADDRESS; //no idea. got it from stackoverflow
        if (TextUtils.isEmpty(ip))
            return false;

        Matcher matcher = patternEmail.matcher(ip);
        return matcher.matches();
    }

}
