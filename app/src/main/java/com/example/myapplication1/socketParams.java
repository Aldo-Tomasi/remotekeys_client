package com.example.myapplication1;

public class socketParams {

        int port;
        String IP_adr;

        socketParams(String IP_adr, int port){
            this.port=port;
            this.IP_adr=IP_adr;

        }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIP_adr() {
        return IP_adr;
    }

    public void setIP_adr(String IP_adr) {
        this.IP_adr = IP_adr;
    }
}
