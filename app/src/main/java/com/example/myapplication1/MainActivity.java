package com.example.myapplication1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button connectBtn = findViewById(R.id.connect_btn); //linkan butun sa onin u xmlu
        final Button muteBtn = findViewById(R.id.MuteBtn);
        final Button playBtn = findViewById(R.id.play_btn);
        final Button nextBtn = findViewById(R.id.next_btn);
        final Button prevBtn = findViewById(R.id.previous_btn);
        final Button volUpBtn = findViewById(R.id.VolUpBtn);
        final Button volDownBtn = findViewById(R.id.VolDownBtn);

        final EditText ipAdrInput = findViewById(R.id.ipAdrInput); //linkan edit text

        muteBtn.setEnabled(false);
        playBtn.setEnabled(false);
        nextBtn.setEnabled(false);
        prevBtn.setEnabled(false);
        volUpBtn.setEnabled(false);
        volDownBtn.setEnabled(false);

        connectBtn.setOnClickListener(new View.OnClickListener() {  //krenu san pisat btn.setOnClickListener(new View.OnClickListener) i on je generirira override
            @Override
            public void onClick(View view) {


                if(Klijent.validateIP(ipAdrInput.getText().toString())==true) {

                    //EXPERIMENTAL


                    String ipAdresa = String.valueOf(ipAdrInput.getText());
                    int port = 1906;

                    socketParams socketParameters = new socketParams(ipAdresa,port);

                    //new AsyncClientConn().execute(String.valueOf(ipAdrInput.getText()),FileUtils.readFileToString(portfile,"UTF-8"));
                    new AsyncClientConn().execute(socketParameters);

                    //OLD
                    // new AsyncClientConn().execute(String.valueOf(ipAdrInput.getText()));


                    Toast.makeText(getApplicationContext(),"connecting to "+String.valueOf(ipAdrInput.getText()),Toast.LENGTH_SHORT).show();

                    muteBtn.setEnabled(true);
                    playBtn.setEnabled(true);
                    nextBtn.setEnabled(true);
                    prevBtn.setEnabled(true);
                    volUpBtn.setEnabled(true);
                    volDownBtn.setEnabled(true);

                        }
                else if(!Klijent.validateIP(ipAdrInput.getText().toString())){
                    Toast.makeText(getApplicationContext(),"Invalid IP address! ",Toast.LENGTH_SHORT).show();
                }


            }
        });


        muteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("6");
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("1");
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("2");
            }
        });

        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("3");
            }
        });

        volUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("4");
            }
        });

        volDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncSendMessage().execute("5");
            }
        });

    }

}
